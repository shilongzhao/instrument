CREATE DATABASE IF NOT EXISTS `instrument_db`;

USE instrument_db;

DROP TABLE IF EXISTS `instrument`;

CREATE TABLE IF NOT EXISTS `instrument`(
    `isin` CHAR(12) NOT NULL PRIMARY KEY,
    `version` INT NOT NULL DEFAULT 0,
    `description` char(255) NOT NULL
);
