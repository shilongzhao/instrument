package org.szhao.instrument.partners

import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.eventbus.deliveryOptionsOf
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import org.apache.logging.log4j.kotlin.Logging
import org.szhao.instrument.RedisClientVerticle
import org.szhao.instrument.util.eventBus

class InstrumentsClientVerticle : CoroutineVerticle() {
  companion object: Logging

  override suspend fun start() {
    val client = vertx.createHttpClient()
    val partnerConf: JsonObject = config["partner"]
    val port: Int = partnerConf["port"]
    val host: String = partnerConf["host"]
    val socket = client.webSocket(port, host, "/instruments").await()

    socket.handler {
      val json = it.toJsonObject();
      logger.debug { "received instrument ${json.encode()}" }
      /**
       * fire and forget
       */
      vertx.eventBus.send(RedisClientVerticle.ADDRESS, json["data"], actionOf(json["type"]))
    }

    logger.info { "instruments client verticle started" }
  }

  private fun actionOf(action: String) = deliveryOptionsOf(headers = mapOf("action" to action))

}
