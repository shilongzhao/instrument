package org.szhao.instrument

import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.array
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import io.vertx.redis.client.Redis
import io.vertx.redis.client.RedisAPI
import io.vertx.redis.client.RedisOptions
import kotlinx.coroutines.launch
import org.apache.logging.log4j.kotlin.Logging
import org.szhao.instrument.util.*
import java.math.BigDecimal


class RedisClientVerticle : CoroutineVerticle(), Logging {
  companion object : Logging {
    const val ADDRESS = "instrument.redis"
    const val MS = 1L
    const val SEC = 1000 * MS
    const val MIN = 60 * SEC
  }


  /**
   * {isin} description
   * current:{isin} price
   * candle:{isin} - {ordered set of candle, by candle start minute time}
   * queue:{isin}:{minute} - {a queue of json objects, ordered by timestamp}
   *
   */
  override suspend fun start() {
    val redisConf: JsonObject = config["redis"]
    val host: String = redisConf["host"]
    val port: Int = redisConf["port"]
    val connection = "redis://$host:$port"
    logger.info { "connecting to redis $connection" }
    val redisOptions = RedisOptions().setConnectionString(connection).setMaxPoolSize(16).setMaxPoolWaiting(64)
    val client = Redis.createClient(vertx, redisOptions).connect {
      if (it.succeeded()) {
        logger.debug { "Redis connection OK: ${it.result()}" }
      } else {
        logger.debug { "Redis connection K.O.: ${it.cause().message}" }
      }
    }

    val redis = RedisAPI.api(client)

    vertx.eventBus.consumer<JsonObject>(ADDRESS) {
      launch {
        dispatch(it, redis)
      }
    }

    logger.info { "redis client verticle started" }
  }


  /**
   * dispatch messages to corresponding handlers
   */
  private suspend fun dispatch(message: Message<JsonObject>, client: RedisAPI) {
    when (message.headers["action"]) {
      "QUOTE" -> {
        logger.debug { "QUOTE ${message.body}" }
        eitherHandler(client, message, this::onQuoteReceived)
      }
      "ADD" -> {
        logger.info { "ADD ${message.body}" }
        eitherHandler(client, message, this::addInstrument)
      }
      "DELETE" -> {
        logger.info { "DELETE ${message.body}" }
        eitherHandler(client, message, this::deleteInstrument)
      }
      "GET_CANDLES" -> {
        logger.debug { "GET_CANDLES ${message.body}" }
        eitherHandler(client, message, this::getCandles)
      }
      "GET_INSTRUMENTS" -> {
        logger.debug { "GET_INSTRUMENTS ${message.body}" }
        eitherHandler(client, message, this::getInstruments)
      }
      "FLUSH_ALL" -> {
        logger.warn { "FLUSH_ALL keys in Redis" }
        eitherHandler(client, message, this::flushAll)
      }
      "BATCH_PROCESS" -> {
        logger.debug { "BATCH PROCESS" }
        eitherHandler(client, message, this::batchProcess)
      }
      else -> {
        logger.debug { "UNKNOWN ${message.body}" }
        message.fail( "Unknown message ${message.body}")
      }
    }
  }


  /**
   * A batch job that run every second for each ISIN
   * 1. read quotes from queue:$isin
   * 2. group these quotes by the minute at which they arrive
   * 3. for each group of quotes, update the candle
   */
  private suspend fun batchProcess(redis: RedisAPI, info: JsonObject) {
    logger.debug { "starting batch process queue:$info" }
    val timestamp = System.currentTimeMillis()
    val list = redis.getAllISINs()
    list.forEach {
      redis.batchProcess2(it, timestamp)
    }
  }


  private suspend fun flushAll(redis: RedisAPI, args: JsonObject): String {
    logger.debug { "flushing all with args $args" }
    return redis.flushall(emptyList()).await().toString()
  }

  /**
   * on quote message received:
   * 1. update current price, i.e. current:isin
   * 2. put it to the ordered set, i.e. queue:isin
   */
  private suspend fun onQuoteReceived(redis: RedisAPI, quote: JsonObject) {
    val timestamp = System.currentTimeMillis()
    val isin: String = quote["isin"]
    val price: String = quote.getString("price")
    redis.updatePrice(isin, price)
    redis.addQuoteToQueue(isin, BigDecimal(price), timestamp)
  }

  private suspend fun getInstruments(redis: RedisAPI, cmd: JsonObject): JsonArray {
    val instruments = redis.getInstruments()
    logger.info { "got ${instruments.size} instruments with $cmd" }
    return json { array(instruments.map(Instrument::toJson)) }
  }

  private suspend fun getCandles(redis: RedisAPI, cmd: JsonObject): JsonArray {
    val candles = redis.getCandlesOf(cmd["isin"])
    logger.info { "got ${candles.size} candles for $cmd" }
    return json { array(candles.map(Candle::toJson)) }
  }

  /**
   * delete instrument by key and also the related data structures
   */
  private suspend fun deleteInstrument(redis: RedisAPI, instrument: JsonObject) {
    val isin: String = instrument["isin"]
    redis.deleteInstrument(isin)
  }


  /**
   * add instrument in Redis
   * key: isin, value: description
   */
  private suspend fun addInstrument(redis: RedisAPI, instrument: JsonObject) {
    val isin: String = instrument["isin"]
    redis.addInstrument(isin, instrument["description"])
  }

  /**
   * handle the exceptions at the top level to avoid boiler plates in sub functions
   */
  private suspend fun <T, R> eitherHandler(
    redis: RedisAPI,
    message: Message<T>,
    handle: suspend (RedisAPI, T) -> R
  ) =
    when (val result = coroutineTry { handle(redis, message.body) }) {
      is Left -> {
        logger.error { "failed request ${message.body}, error ${result.cause.message}" }
        message.fail(-1, result.cause.message)
      }
      is Right -> {
        logger.debug { "successful get result ${result.value} for handle $handle" }
        message.reply(result.value)
      }
    }

}
