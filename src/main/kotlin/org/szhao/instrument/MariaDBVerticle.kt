package org.szhao.instrument

import io.vertx.core.json.JsonObject
import io.vertx.jdbcclient.JDBCPool
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.jdbcclient.jdbcConnectOptionsOf
import io.vertx.kotlin.sqlclient.poolOptionsOf
import org.apache.logging.log4j.kotlin.Logging
import org.szhao.instrument.util.eventBus
import java.util.concurrent.TimeUnit

class MariaDBVerticle : CoroutineVerticle(), Logging {

  override suspend fun start() {
    val jdbcConf: JsonObject = config["jdbc"]

    val poolOptions = poolOptionsOf(
      idleTimeout = 100,
      idleTimeoutUnit = TimeUnit.MILLISECONDS,
      maxSize = 10,
      maxWaitQueueSize = 100
    ) // TODO: read from conf

    val connectOptions = jdbcConnectOptionsOf(
      user = jdbcConf["user"],
      password = jdbcConf["password"],
      jdbcUrl = jdbcConf["url"]
    )
    logger.debug { "connect options ${connectOptions.toJson().encode()}" }

    val pool = JDBCPool.pool(vertx, connectOptions, poolOptions)

    pool.query("SELECT * FROM instrument").execute().onComplete {
      if (it.succeeded()) {
        logger.info { "Database connection OK: ${it.result()}" }
      } else {
        logger.info { "Database connection KO: ${it.cause().message}" }
      }
    }

    vertx.eventBus.consumer<JsonObject> ("ADDRESS").handler {
      TODO("persist delete operation and add operation/candles")
    }

  }
}
