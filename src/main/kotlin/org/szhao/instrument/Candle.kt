package org.szhao.instrument

import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.jsonObjectOf
import java.math.BigDecimal
import java.time.Instant
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.concurrent.TimeUnit


data class Instrument(val isin: String, val description: String, val price: String?) {
  fun toJson(): JsonObject = jsonObjectOf(
    "isin" to isin, "description" to description, "price" to price
  )
}


data class Candle(
  val isin: String,
  val openTimestampInternal: Long, val openTimestamp: String, val openPrice: BigDecimal,
  val highPrice: BigDecimal, val lowPrice: BigDecimal,
  val closeTimestampInternal: Long, val closeTimestamp: String, val closePrice: BigDecimal
) {
  companion object {
    fun fromMap(map: Map<String, String>): Candle {
      if (map.isEmpty()) throw RuntimeException("empty map cannot converted to Candle")

      val isin: String = map["isin"]!!
      val openTimestampInternal = BigDecimal(map["openTimestampInternal"]).toLong()
      val openTimeStamp = openTimestampOf(openTimestampInternal)
      val openPrice = BigDecimal(map["openPrice"])
      val highPrice = BigDecimal(map["highPrice"])
      val lowPrice = BigDecimal(map["lowPrice"])
      val closePrice = BigDecimal(map["closePrice"])
      val closeTimestampInternal = BigDecimal(map["closeTimestampInternal"]).toLong()
      val closeTimestamp = closeTimestampOf(closeTimestampInternal)
      return Candle(
        isin, openTimestampInternal, openTimeStamp, openPrice,
        highPrice, lowPrice,
        closeTimestampInternal, closeTimestamp, closePrice
      )
    }

    fun fromSingleQuote(quote: Quote): Candle =
      Candle(
        quote.isin, quote.timestamp, openTimestampOf(quote.timestamp), quote.price,
        quote.price, quote.price,
        quote.timestamp, closeTimestampOf(quote.timestamp), quote.price
      )

  }

  fun toJson(): JsonObject = jsonObjectOf(
    "isin" to isin, "openTimestamp" to openTimestamp, "openPrice" to openPrice,
    "highPrice" to highPrice, "lowPrice" to lowPrice,
    "closePrice" to closePrice, "closeTimestamp" to closeTimestamp
  )

}

data class Quote(val isin: String, val price: BigDecimal, val timestamp: Long) {
  companion object {
    fun fromMap(map: Map<String, String>): Quote {
      if (map.isEmpty()) throw RuntimeException("empty map cannot converted to Quote")

      val isin: String = map["isin"]!!
      val timestamp = map["timestamp"]!!.toLong()
      val price = BigDecimal(map["price"])
      return Quote(isin, price, timestamp)
    }
  }
}

/**
 * merge a quote into a candle
 */
private fun merge(quote: Quote, candle: Candle): Candle {
  var result = candle
  if (candle.isin != quote.isin) throw RuntimeException("invalid merge between two different isin!")

  // update open time, open price
  if (quote.timestamp < candle.openTimestampInternal) {
    result = result.copy(
      openTimestampInternal = quote.timestamp,
      openTimestamp = openTimestampOf(quote.timestamp),
      openPrice = quote.price
    )
  }

  // update high price
  if (candle.highPrice < quote.price) {
    result = result.copy(
      highPrice = quote.price
    )
  }

  // update low price
  if (quote.price < candle.lowPrice) {
    result = result.copy(
      lowPrice = quote.price
    )
  }

  // update close time
  if (quote.timestamp > candle.closeTimestampInternal) {
    result = result.copy(
      closeTimestamp = closeTimestampOf(quote.timestamp),
      closeTimestampInternal = quote.timestamp,
      closePrice = quote.price
    )
  }

  return result
}

/**
 * To reduce the frequency of writings into Redis, we aggregate some quotes in the queue
 * Return a map of partial candle
 */
fun aggregateQuotesToCandle(quotes: List<Quote>): Candle {
  if (quotes.isEmpty()) throw RuntimeException("empty quote list!")
  val candle = Candle.fromSingleQuote(quotes[0])
  val minute = minutesSinceEpoch(quotes[0].timestamp)
  return quotes.foldRight(candle) { quote, c ->
    if (minute != minutesSinceEpoch(quote.timestamp))
      throw RuntimeException("quotes must be of the same minute!")
    merge(quote, c)
  }
}

/**
 * zoned data time of
 */
fun zdtOf(timestamp: Long): ZonedDateTime {
  val instant = Instant.ofEpochMilli(timestamp)
  return ZonedDateTime.ofInstant(instant, ZoneOffset.UTC)
}

fun minutesSinceEpoch(timestamp: Long): Long {
  return TimeUnit.MILLISECONDS.toMinutes(timestamp)
}

private fun formatTimestamp(timestamp: Long, formatter: DateTimeFormatter) =
  formatter.format(zdtOf(timestamp))

fun openTimestampOf(timestamp: Long): String =
  formatTimestamp(timestamp, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:00"))

fun closeTimestampOf(timestamp: Long): String =
  formatTimestamp(timestamp + 60 * RedisClientVerticle.SEC, DateTimeFormatter.ofPattern("HH:mm:00"))


