package org.szhao.instrument

import io.vertx.config.ConfigRetriever
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.config.configRetrieverOptionsOf
import io.vertx.kotlin.config.configStoreOptionsOf
import io.vertx.kotlin.core.deploymentOptionsOf
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import org.apache.logging.log4j.kotlin.Logging
import org.szhao.instrument.partners.InstrumentsClientVerticle
import org.szhao.instrument.partners.QuotesClientVerticle

class MainVerticle : CoroutineVerticle(), Logging {
  companion object {
  }

  override suspend fun start() {

    val conf = if (!config.isEmpty) config else readConf(env["ZHAO_CONF_FILE"])
    logger.debug{"application configuration: $conf" }

    /**
     * synchronous and sequential start up of verticles
     */
    vertx.deployVerticle(RedisClientVerticle::class.java, deploymentOptionsOf(config = conf, instances = 5)).await()
    vertx.deployVerticle(MariaDBVerticle::class.java, deploymentOptionsOf(config = conf)).await()
    vertx.deployVerticle(HttpServerVerticle::class.java, deploymentOptionsOf(config = conf)).await()
    vertx.deployVerticle(SchedulerVerticle::class.java, deploymentOptionsOf(config = conf)).await()

    val environment: String = conf["env"]
    if (environment != "test") {
      vertx.deployVerticle(InstrumentsClientVerticle::class.java, deploymentOptionsOf(config = conf)).await()
      vertx.deployVerticle(QuotesClientVerticle::class.java, deploymentOptionsOf(config = conf)).await()
    }
    logger.info("system started")
  }

  /**
   * read configuration file
   */
  private suspend fun readConf(filename: String?): JsonObject {
    logger.debug("configuration file path=$filename")
    val localJsonStore = configStoreOptionsOf(jsonObjectOf("path" to filename), type = "file", format = "hocon", optional = false)
    val retrieverOptions = configRetrieverOptionsOf(stores = listOf(localJsonStore))
    val configRetriever = ConfigRetriever.create(vertx, retrieverOptions)
    return configRetriever.config.await()
  }

  /**
   * shorthand for System environment variables
   */
  private val env: Map<String, String>
    get() = System.getenv()
}
