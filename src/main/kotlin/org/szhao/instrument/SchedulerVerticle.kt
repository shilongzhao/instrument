package org.szhao.instrument

import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.eventbus.deliveryOptionsOf
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.coroutines.CoroutineVerticle
import org.apache.logging.log4j.kotlin.Logging
import org.szhao.instrument.RedisClientVerticle.Companion.SEC

class SchedulerVerticle: CoroutineVerticle(), Logging {
  override suspend fun start() {
    val redisConf: JsonObject = config["redis"]

    val batchProcess: Boolean = redisConf.getBoolean("batchProcess", true)
    if (batchProcess) {
      vertx.setPeriodic(15 * SEC) {
        vertx.eventBus().send(RedisClientVerticle.ADDRESS, jsonObjectOf(), actionOf("BATCH_PROCESS"))
      }
    }
    logger.debug { "scheduler verticle deployed" }
  }

  private fun actionOf(action: String) = deliveryOptionsOf(headers = mapOf("action" to action))

}
