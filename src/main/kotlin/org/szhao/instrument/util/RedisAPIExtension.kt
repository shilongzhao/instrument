package org.szhao.instrument.util

import io.vertx.kotlin.coroutines.await
import io.vertx.redis.client.RedisAPI
import org.apache.logging.log4j.kotlin.logger
import org.szhao.instrument.*
import org.szhao.instrument.RedisClientVerticle.Companion.MIN
import java.math.BigDecimal
import kotlin.math.max
import kotlin.math.min

suspend fun RedisAPI.getInstruments(): List<Instrument> {
  val descriptions = hgetall("descriptions").await()
  val descMap = descriptions.keys.associateWith { descriptions[it].toString() }

  val prices = hgetall("prices").await()
  val priceMap = prices.keys.associateWith { prices[it].toString() }

  return descMap.map { (isin, desc) ->
    Instrument(isin, desc, priceMap[isin])
  }.toList()
}

suspend fun RedisAPI.getCandlesOf(isin: String, length: Int = 30): List<Candle> {
  val keys = candleKeysOf(isin).reversed().take(length)
  return keys.map {
    // heavy IO, to do better, MGET?
    getCandleByKey(it)
  }
}

/**
 * Redis price:isin
 */
suspend fun RedisAPI.updatePrice(isin: String, price: BigDecimal) {
  updatePrice(isin, price.toPlainString())
}

/**
 * use a hash named `prices` to store prices, use ISIN as key
 */
suspend fun RedisAPI.updatePrice(isin: String, price: String): Boolean {
  if (!containsISIN(isin)) {
    logger("updatePrice").info { "no instrument found for ISIN $isin, price update $price is ignored" }
    return false
  }

  val r = hset(listOf("prices", isin, price)).await()
  return r.toBoolean()
}

suspend fun RedisAPI.getPrice(isin: String): BigDecimal? {
  if (!containsISIN(isin)) return null

  return BigDecimal(hget("prices", isin).await().toString())
}

suspend fun RedisAPI.getDescription(isin: String): String? {
  if (!containsISIN(isin))
    return null
  val resp = hget("descriptions", isin).await()
  return resp.toString()
}

/**
 * use a hash named `descriptions` to store descriptions, use ISIN as key
 */
suspend fun RedisAPI.addInstrument(isin: String, description: String): Boolean {
  var success = sadd(listOf("instruments", isin)).await().toBoolean()
  if (!success) {
    return false
  }
  success = hset(listOf("descriptions", isin, description)).await().toBoolean()
  return success
}

suspend fun RedisAPI.getAllISINs(): List<String> {
  return smembers("instruments").await().map { it.toString() }
}

/**
 * we use a set `instruments` to remember all the ISINs
 */
suspend fun RedisAPI.containsISIN(isin: String): Boolean {
  val resp = sismember("instruments", isin).await()
  return resp.toBoolean()
}

/**
 * maintain a queue for each ISIN, score is timestamp
 * the messages are ordered by the timestamp when they arrived
 */
suspend fun RedisAPI.addQuoteToQueue(isin: String, price: BigDecimal, timestamp: Long) {
  addQuoteToQueue(isin, price.toPlainString(), timestamp)
}

suspend fun RedisAPI.addQuoteToQueue(isin: String, price: String, timestamp: Long) {
  val score = timestamp.toBigDecimal().toPlainString()
  // since it's a set it has no duplicate prices - we append price with timestamp - so it's unique
  zadd(listOf("queue:$isin", score, "$price:$score")).await()
}

/**
 * returns a list of maps of price and timestamp for the given ISIN
 */
suspend fun RedisAPI.getQuoteBefore(isin: String, timestamp: Long): List<Quote> {
  val score = timestamp.toBigDecimal().toPlainString()
  val resp = zrangebyscore(listOf("queue:$isin", "0", score, "WITHSCORES")).await()

  return resp.map {
    mapOf(
      "isin" to isin,
      "price" to it[0].toString().split(":")[0],
      "timestamp" to it[1].toLong().toBigDecimal().toPlainString() // to avoid scientific notation
    )
  }.map { Quote.fromMap(it) }
}

suspend fun RedisAPI.removeQuotesBefore(isin: String, timestamp: Long) {
  zremrangebyscore("queue:$isin", "0", timestamp.toBigDecimal().toPlainString()).await()
}

/**
 * delete instrument:
 * - remove from `instruments`
 * - remove from `descriptions`
 * - remove from `prices`
 * - remove `queue:isin'
 * - remove `candle:isin:*`
 */
suspend fun RedisAPI.deleteInstrument(isin: String) {
  srem(listOf("instruments", isin)).await()
  hdel(listOf("descriptions", isin)).await()
  hdel(listOf("prices", isin)).await()
  del(listOf("queue:$isin")).await()
  val keys = candleKeysOf(isin)
  if (keys.isEmpty()) {
    return
  }
  del(keys).await()
  logger("deleteInstrument").info { "delete from candles OK $isin" }

}

/**
 * get the corresponding candle of an ISIN and it's timestamp
 * returns result in a map
 */
suspend fun RedisAPI.getCandle(isin: String, timestamp: Long): Candle? {
  val minute = minutesSinceEpoch(timestamp)
  val key = "candle:$isin:$minute"
  val candle = hgetall(key).await()
  logger("getCandle").info { "candle keys: ${candle.keys} " }
  val map = candle.keys.associateWith { candle[it].toString() }
  if (map.isEmpty())
    return null
  return Candle.fromMap(map)
}

/**
 * it's new candle
 */
suspend fun RedisAPI.insertCandle(candle: Candle) {
  val minute = minutesSinceEpoch(candle.openTimestampInternal)
  val isin = candle.isin
  val key = "candle:$isin:$minute"
  hset(
    listOf(
      key,
      "isin", isin, // internal use
      "openTimestamp", candle.openTimestamp,
      "openTimestampInternal", candle.openTimestampInternal.toBigDecimal().toPlainString(), // internal use
      "openPrice", candle.openPrice.toPlainString(),
      "highPrice", candle.highPrice.toPlainString(),
      "lowPrice", candle.lowPrice.toPlainString(),
      "closePrice", candle.closePrice.toPlainString(),
      "closeTimestampInternal", candle.closeTimestampInternal.toBigDecimal().toPlainString(), // internal use
      "closeTimestamp", candle.closeTimestamp
    )
  ).await()

}

suspend fun RedisAPI.candleKeysOf(isin: String): List<String> {
  val resp = keys("candle:$isin:*").await().map { it.toString() }
  return resp.sorted().toList()
}

suspend fun RedisAPI.getCandleByKey(key: String): Candle {
  val candle = hgetall(key).await()
  val map = candle.keys.associateWith { candle[it].toString() }
  return Candle.fromMap(map)
}

private suspend fun RedisAPI.addGapCandle(isin: String, price: BigDecimal, timestamp: Long) {
  val candle = Candle.fromSingleQuote(Quote(isin, price, timestamp))
  insertCandle(candle)
}

suspend fun RedisAPI.fillGapCandles(isin: String, price: BigDecimal, minuteFrom: Long, minuteTo: Long) {
  (minuteFrom..minuteTo).forEach {
    addGapCandle(isin, price, it * MIN)
    logger("fillGapCandles").info { "add gap candle for key 'candle:$isin:$it'" }
  }
}

/**
 * update or insert candle
 */
suspend fun RedisAPI.upsertCandle(partial: Candle) {
  val isin = partial.isin
  val timestamp = partial.openTimestampInternal
  val existing = getCandle(isin, timestamp)

  if (existing == null) {
    val allKeys = candleKeysOf(isin)
    if (allKeys.isEmpty()) { // first since epoch
      insertCandle(partial)
    } else {
      val lastKey = allKeys.last()
      val lastCandle = getCandleByKey(lastKey)
      val lastCandleMinute = lastKey.split(":").last().toLong()
      val currentMinute = minutesSinceEpoch(timestamp)

      fillGapCandles(isin, lastCandle.closePrice, lastCandleMinute + 1, currentMinute - 1)
      val toPersist = partial.copy(openPrice = lastCandle.closePrice)
      insertCandle(toPersist)
    }
  } else {
    insertCandle(combineCandle(existing, partial))
  }
}

fun combineCandle(one: Candle, other: Candle): Candle {
  if (one.isin != other.isin || one.openTimestamp != other.openTimestamp || one.closeTimestamp != other.closeTimestamp) {
    throw RuntimeException("Cannot merge two candles of different minutes or ISINs")
  }

  return one.copy(
    openTimestampInternal = min(one.openTimestampInternal, other.openTimestampInternal),
    openPrice = if (one.openTimestampInternal < other.closeTimestampInternal) one.openPrice else other.openPrice,
    closeTimestampInternal = max(one.closeTimestampInternal, other.closeTimestampInternal),
    closePrice = if (one.closeTimestampInternal > other.closeTimestampInternal) one.closePrice else other.closePrice,
    highPrice = one.highPrice.max(other.highPrice),
    lowPrice = one.lowPrice.min(other.lowPrice)
  )
}


suspend fun RedisAPI.batchProcess(isin: String, timestamp: Long) {
  // read quotes arrived in the last second
  val quotes = getQuoteBefore(isin, timestamp)
  // group the quotes by the minute they belong to
  val quotesByMinute = quotes.groupBy { q -> minutesSinceEpoch(q.timestamp) }
  // update existing candle or insert the new candle
  quotesByMinute.forEach { (_, quotesOfTheMinute) ->
    val partialCandle = aggregateQuotesToCandle(quotesOfTheMinute)
    upsertCandle(partialCandle)
  }
  // remove processed quotes from queue, note that actually we need to delete exactly the ones in
  removeQuotesBefore(isin, timestamp)
}


/**
 * compared to `batchProcess`, this method does not fill gaps, we fill the gap as soon as possible, not
 * delayed until an arrive of quote like the behaviour in `batchProcess`
 */
suspend fun RedisAPI.batchProcess2(isin: String, timestamp: Long) {
  val candle = getCandle(isin, timestamp)
  // read quotes arrived in the last second
  val quotes = getQuoteBefore(isin, timestamp)
  // remove processed quotes from queue, note that actually we need to delete exactly the ones in
  removeQuotesBefore(isin, timestamp)
  // group the quotes by the minute they belong to
  val quotesByMinute = quotes.groupBy { q -> minutesSinceEpoch(q.timestamp) }

  val minute = minutesSinceEpoch(timestamp)

  if (candle == null) { // no candle of current minute yet
    val keys = candleKeysBefore(isin, minute)
    if (keys.isEmpty()) {
      logger("batchProcess2").debug { "no previous candle for $isin" }
      if (quotesByMinute.containsKey(minute)) {
        val aggregated = aggregateQuotesToCandle(quotesByMinute[minute]!!)
        insertCandle(aggregated)
      } else {
        logger("batchProcess2").debug { "no quotes arrived yet for $isin at $timestamp, do nothing" }
      }
    } else {
      // no candle for current minutes, but we do have some previous candles
      val prev = getCandleByKey(keys.last())
      if (quotesByMinute.containsKey(minute)) {
        // and we got some new arriving quotes
        val aggregated = aggregateQuotesToCandle(quotesByMinute[minute]!!)
        insertCandle(aggregated.copy(openPrice = prev.closePrice))
      } else {
        // we get no candle for current minute, but having a prev candle, and no arriving quotes yet,
        // then we just create a candle using prev close price as opening price
        insertCandle(Candle.fromSingleQuote(Quote(isin, prev.closePrice, minute * MIN)))
      }
    }
  } else {
    // we have an existing candle for current minute
    if (quotesByMinute.containsKey(minute)) {
      val aggregated = aggregateQuotesToCandle(quotesByMinute[minute]!!)
      insertCandle(combineCandle(candle, aggregated))
    } else {
      logger("batchProcess2").info{"no need to update candle of $isin, since no quote has arrived yet"}
    }
  }
}

suspend fun RedisAPI.candleKeysBefore(isin: String, minutesSinceEpoch: Long): List<String> {
  val keys = candleKeysOf(isin);
  return keys.takeWhile {
    it.split(":")[2].toLong() < minutesSinceEpoch
  }
}

