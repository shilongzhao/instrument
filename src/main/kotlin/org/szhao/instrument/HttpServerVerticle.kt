package org.szhao.instrument

import io.vertx.core.AsyncResult
import io.vertx.core.Handler
import io.vertx.core.Promise
import io.vertx.core.eventbus.DeliveryOptions
import io.vertx.core.eventbus.EventBus
import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Route
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.kotlin.core.eventbus.deliveryOptionsOf
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.core.json.obj
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.coroutines.awaitResult
import kotlinx.coroutines.launch
import org.apache.logging.log4j.kotlin.Logging
import org.szhao.instrument.util.eventBus

class HttpServerVerticle : CoroutineVerticle(), Logging {
  override suspend fun start() {
    val router = Router.router(vertx)
    router.errorHandler(500) {
      it.response().end(json { obj("error" to it.failure().message).encode() })
    }
    router.get("/candles/:isin").coroutineHandler(this::getCandles)
    router.get("/instruments").coroutineHandler(this::getInstruments)
    vertx.createHttpServer().requestHandler(router).listen(8080).await()
    logger.info { "server started on localhost:8080" }
  }

  private suspend fun getCandles(context: RoutingContext) {
    val id = context.pathParam("isin")
    logger.info { "getting candles for $id" }
    val cmd = jsonObjectOf("isin" to id)
    val actionOf = actionOf("GET_CANDLES")
    vertx.eventBus.request<JsonArray>(RedisClientVerticle.ADDRESS, cmd, actionOf)
      .onSuccess {
        val candles = it.body()
        logger.info { "got ${candles.size()} candles for $id" }
        context.response().end(candles.encode())
      }.onFailure {
        logger.error { "error while getting candles for $id: ${it.message}" }
        context.response().end(jsonObjectOf("error" to it.message).encode())
      }

//    val promise = Promise.promise<String>()
//    promise.complete("hello")
//    promise.future().await()
  }

//  fun <T> map(f0: (Handler<AsyncResult<T>>) -> Unit): suspend () -> T = {
//    val promise = Promise.promise<T>()
//    f0(promise)
//    promise.future().await()
//  }
//
//  fun <A, T> map1(f1: OneParamWithHandlerFunction<A, T>): suspend (A) -> T = {
//    val promise = Promise.promise<T>()
//    f1(it, promise)
//    promise.future().await()
//  }
//
//  fun <A, B, T> map2(f2: TwoParamWithHandlerFunction<A, B, T>): suspend (A, B) -> T = { a, b ->
//    val promise = Promise.promise<T>()
//    f2(a, b, promise)
//    promise.future().await()
//  }
//
//
//  fun <A, B, C, T> map3(f3: ThreeParamWithHandlerFunction<A, B, C, T>): suspend (A, B, C) -> T = { a, b, c ->
//    val promise = Promise.promise<T>()
//    f3(a, b, c, promise)
//    promise.future().await()
//  }

  private suspend fun getInstruments(context: RoutingContext) {
    val action = actionOf("GET_INSTRUMENTS")

    val message = vertx.eventBus.awaitRequest<JsonArray>(RedisClientVerticle.ADDRESS, jsonObjectOf(), action)
//    throw RuntimeException("run time exception")
    // router error handler will catch the exceptions for us, thanks to the coroutineHandler
    val instruments = message.body()
    logger.info { "got ${instruments.size()} of instruments" }
    context.response().end(instruments.encode())
  }


  private fun Route.coroutineHandler(fn: suspend (RoutingContext) -> Unit) =
    handler {
      // we can use `launch` here, because we are in side a CoroutineScope,
      // our verticle extends CoroutineVerticle which in turn implements CoroutineScope,
      // thus here we are inside it (but our verticle also has a coroutineScope as context)
      launch {
        try {
          fn(it)
        } catch (e: Exception) {
          when(e) {
            is RuntimeException -> it.fail(500, e)
            else -> it.fail(400, e)
          }
        }
      }
    }


  private fun actionOf(action: String) = deliveryOptionsOf(headers = mapOf("action" to action))

}

typealias AsyncHandler<T> = Handler<AsyncResult<T>>
typealias OneParamWithHandlerFunction<A, T> = (a: A, handler: AsyncHandler<T>) -> Unit
typealias TwoParamWithHandlerFunction<A, B, T> = (a: A, b: B, handler: AsyncHandler<T>) -> Unit
typealias ThreeParamWithHandlerFunction<A, B, C, T> = (a: A, b: B, c: C, handler: AsyncHandler<T>) -> Unit

suspend fun <T> EventBus.coRequest(address: String, message: Any?, deliveryOptions: DeliveryOptions): Message<T> {
  val p = Promise.promise<Message<T>>()
  request(address, message, deliveryOptions, p)
  return p.future().await()
}

suspend fun <T> EventBus.awaitRequest(address: String, message: Any?, deliveryOptions: DeliveryOptions): Message<T> =
  awaitResult {
    request(address, message, deliveryOptions, it)
  }
//
//fun <A, B, C, T> map3(f3: ThreeParamWithHandlerFunction<A, B, C ,T>): suspend (A, B, C) -> T = { a, b, c ->
//  awaitResult {
//    f3(a, b, c, it)
//  }
//}
