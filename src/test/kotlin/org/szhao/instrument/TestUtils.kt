package org.szhao.instrument

import io.vertx.config.ConfigRetriever
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.config.configRetrieverOptionsOf
import io.vertx.kotlin.config.configStoreOptionsOf
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.coroutines.await

suspend fun readTestConfiguration(vertx: Vertx): JsonObject {
  val filename = "test.conf"
  val localJsonStore = configStoreOptionsOf(jsonObjectOf("path" to filename), type = "file", format = "hocon", optional = false)
  val retrieverOptions = configRetrieverOptionsOf(stores = listOf(localJsonStore))
  val configRetriever = ConfigRetriever.create(vertx, retrieverOptions)
  return configRetriever.config.await()
}
