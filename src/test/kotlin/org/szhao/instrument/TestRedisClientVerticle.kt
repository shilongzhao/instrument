package org.szhao.instrument

import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import io.vertx.kotlin.core.deploymentOptionsOf
import io.vertx.kotlin.core.eventbus.deliveryOptionsOf
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.runBlocking
import org.apache.logging.log4j.kotlin.Logging
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.szhao.instrument.util.body
import org.szhao.instrument.util.eventBus

@ExtendWith(VertxExtension::class)
class TestRedisClientVerticle : Logging {
  companion object {
    @BeforeAll
    @JvmStatic
    fun startSystem(vertx: Vertx, context: VertxTestContext) = runBlocking(vertx.dispatcher()) {
      val sysConf = readTestConfiguration(vertx)
      vertx.deployVerticle(RedisClientVerticle(), deploymentOptionsOf(config = sysConf)).await()
      context.completeNow()
    }
  }

  @BeforeEach
  fun clear(vertx: Vertx, context: VertxTestContext) = runBlocking(vertx.dispatcher()) {
    val r = vertx.eventBus().request<Any?>(RedisClientVerticle.ADDRESS, jsonObjectOf(), actionOf("FLUSH_ALL")).await()
    logger.info { "flush result ${r.body}" }
    context.completeNow()
  }

  @Test
  fun `it should return empty array if no instrument is defined`(vertx: Vertx, context: VertxTestContext) =
    runBlocking(vertx.dispatcher()) {
      val m = vertx.eventBus()
        .request<JsonArray>(RedisClientVerticle.ADDRESS, jsonObjectOf(), actionOf("GET_INSTRUMENTS"))
        .await()

      assertTrue(m.body.isEmpty)
      context.completeNow()
    }

  @Test
  fun `add and then get instruments should return all instruments`(vertx: Vertx, context: VertxTestContext) =
    runBlocking(vertx.dispatcher()) {
      val s = "TB1015351632"
      val json = jsonObjectOf("isin" to s, "description" to "ARKK")
      vertx.eventBus.request<Any?>(RedisClientVerticle.ADDRESS, json, actionOf("ADD")).await()
      val actionOf = actionOf("GET_INSTRUMENTS")
      val message = vertx.eventBus().request<JsonArray>(RedisClientVerticle.ADDRESS, jsonObjectOf(), actionOf).await()
      val instruments = message.body()
      assertFalse(instruments.isEmpty)

      val ins: JsonObject = instruments[0]
      assertEquals(s, ins["isin"])
      assertEquals("ARKK", ins["description"])
      assertNull(ins["price"])
      context.completeNow()
    }

  @Test
  fun `it should return empty array if no candle is defined yet`(vertx: Vertx, context: VertxTestContext) =
    runBlocking(vertx.dispatcher()) {
      val s = "TB1015351632"

      val m = vertx.eventBus().request<JsonArray>(
        RedisClientVerticle.ADDRESS,
        jsonObjectOf("isin" to s),
        actionOf("GET_CANDLES")
      ).await()

      assertTrue(m.body.isEmpty)
      context.completeNow()
    }

  private fun actionOf(action: String) = deliveryOptionsOf(headers = mapOf("action" to action))


}
