package org.szhao.instrument

import io.kotest.property.Arb
import io.kotest.property.arbitrary.stringPattern
import io.kotest.property.forAll
import io.vertx.core.Vertx
import io.vertx.core.http.HttpMethod
import io.vertx.ext.web.client.WebClient
import io.vertx.junit5.VertxExtension
import io.vertx.kotlin.core.deploymentOptionsOf
import io.vertx.kotlin.core.eventbus.deliveryOptionsOf
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.runBlocking
import org.apache.logging.log4j.kotlin.Logging
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.szhao.instrument.util.body


@ExtendWith(VertxExtension::class)
class TestHttpServerVerticle : Logging {
  companion object {
    @BeforeAll
    @JvmStatic
    fun startSystem(vertx: Vertx) {
      runBlocking(vertx.dispatcher()) {
        val sysConf = readTestConfiguration(vertx)
        vertx.deployVerticle(MainVerticle(), deploymentOptionsOf(config = sysConf)).await()
      }
    }
  }

  @BeforeEach
  fun clear(vertx: Vertx) = runBlocking(vertx.dispatcher()) {
    val r = vertx.eventBus().request<Any?>(RedisClientVerticle.ADDRESS, jsonObjectOf(), actionOf("FLUSH_ALL")).await()
    logger.info { "flush result ${r.body}" }
  }

  @Test
  fun `get instruments at beginning should return empty list`(vertx: Vertx) =
    runBlocking(vertx.dispatcher()) {
      val httpClient = WebClient.create(vertx)
      val resp = httpClient.request(HttpMethod.GET, 8080, "localhost", "/instruments")
        .send().await()
      val array = resp.bodyAsJsonArray()
      Assertions.assertTrue(array.isEmpty)
      httpClient.close()
    }

  @Test
  fun `get candles at beginning should return empty list`(vertx: Vertx) =
    runBlocking(vertx.dispatcher()) {
      val httpClient = WebClient.create(vertx)

      forAll(iterations = 10, Arb.stringPattern("[A-Z]{2}[0-9]{9}")) { a ->
        logger.debug { "testing with ISIN $a" }
        val resp = httpClient.request(HttpMethod.GET, 8080, "localhost", "/candles/$a")
          .send().await()
        val array = resp.bodyAsJsonArray()
        array.isEmpty
      }

      httpClient.close()

    }

  private fun actionOf(action: String) = deliveryOptionsOf(headers = mapOf("action" to action))

}
