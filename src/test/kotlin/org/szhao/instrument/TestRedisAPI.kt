package org.szhao.instrument

import io.kotest.property.Arb
import io.kotest.property.arbitrary.*
import io.kotest.property.forAll
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.coroutines.dispatcher
import io.vertx.redis.client.Redis
import io.vertx.redis.client.RedisAPI
import io.vertx.redis.client.RedisOptions
import kotlinx.coroutines.runBlocking
import org.apache.logging.log4j.kotlin.Logging
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.szhao.instrument.RedisClientVerticle.Companion.MIN
import org.szhao.instrument.RedisClientVerticle.Companion.MS
import org.szhao.instrument.RedisClientVerticle.Companion.SEC
import org.szhao.instrument.util.*
import java.math.BigDecimal

typealias GenTimestamp = (Long, Long) -> Arb<Long>

@ExtendWith(VertxExtension::class)
class TestRedisAPI : Logging {

  private val genISIN = Arb.stringPattern("[A-Z]{2}[0-9]{11}")
  private val genDesc = Arb.string(10, 20)
  private val genPrice = Arb.stringPattern("[1-9][0-9]*\\.[0-9]+")
  private val genTimestamp: GenTimestamp = { start: Long, end: Long -> Arb.long(start, end) }

  private val quoteArb ={ gt: GenTimestamp, from: Long, to: Long -> arbitrary { rs ->
    val id = genISIN.next(rs)
    val price = genPrice.next(rs)
    val timestamp = gt(from, to).next(rs)
    Quote(id, BigDecimal(price), timestamp)
  }}

  companion object {
    lateinit var redis: RedisAPI

    @BeforeAll
    @JvmStatic
    fun setup(vertx: Vertx) = runBlocking(vertx.dispatcher()) {
      redis = getRedisAPI(vertx)
    }

    private suspend fun getRedisAPI(vertx: Vertx): RedisAPI {
      val sysConf = readTestConfiguration(vertx)

      val redisConf: JsonObject = sysConf["redis"]
      val host: String = redisConf["host"]
      val port: Int = redisConf["port"]
      val connection = "redis://$host:$port"
      val client = Redis.createClient(vertx, RedisOptions().setConnectionString(connection)).connect().await()

      return RedisAPI.api(client)
    }
  }

  @BeforeEach
  fun clear(vertx: Vertx) {
    runBlocking(vertx.dispatcher()) {
      redis.flushall(emptyList()).await()
    }
  }

  @Test
  fun `add and then get instrument should be equal`(vertx: Vertx): Unit =
    runBlocking(vertx.dispatcher()) {
      forAll(iterations = 10, genISIN, genDesc) { id, desc ->
        logger.debug { "add instrument: $id -> $desc" }
        redis.addInstrument(id, desc)
        val d = redis.getDescription(id)
        d == desc
      }
    }

  @Test
  fun `add instruments should return true`(vertx: Vertx): Unit =
    runBlocking(vertx.dispatcher()) {
      forAll(10, genISIN, genDesc) { isin, desc ->
        redis.addInstrument(isin, desc)
      }
    }

  @Test
  fun `get instruments should be OK`(vertx: Vertx): Unit =
    runBlocking(vertx.dispatcher()) {
      forAll(10, genISIN, genDesc) { isin, desc ->
        redis.addInstrument(isin, desc)
        redis.getInstruments().contains(Instrument(isin, desc, null))
      }
    }

  @Test
  fun `updating price on non exist instrument will take no effect`(vertx: Vertx): Unit =
    runBlocking(vertx.dispatcher()) {
      forAll(10, genISIN, genPrice) { id, price ->
        redis.updatePrice(id, price)
        null == redis.getPrice(id)
      }
    }

  @Test
  fun `update price and get price should be OK`(vertx: Vertx): Unit =
    runBlocking(vertx.dispatcher()) {

      forAll(genISIN, genDesc, genPrice) { id, desc, price ->
        redis.addInstrument(id, desc)
        redis.updatePrice(id, price)

        price == redis.getPrice(id)?.toPlainString()
      }

    }

  @Test
  fun `get all ISINs should return all the inserted ISINs`(vertx: Vertx, context: VertxTestContext) =
    runBlocking(vertx.dispatcher()) {

      redis.apply {
        addInstrument("TB1015351631", "ark k")
        addInstrument("TB1015351632", "ark g")
        addInstrument("TB1015351633", "ark f")
        addInstrument("TB1015351634", "ark x")
      }
      val list = redis.getAllISINs()


      assertTrue(list.contains("TB1015351631"))
      assertTrue(list.contains("TB1015351632"))
      assertTrue(list.contains("TB1015351633"))
      assertTrue(list.contains("TB1015351634"))
      context.completeNow()
    }

  @Test
  fun `add a quote to empty queue should be OK`(vertx: Vertx, context: VertxTestContext) =
    runBlocking(vertx.dispatcher()) {
      val timestamp = 1621805854000L // Date and time (GMT): Sunday, May 23, 2021 9:37:34 PM

      redis.addQuoteToQueue("TB1015351632", BigDecimal("3.1234"), timestamp)
      val list = redis.getQuoteBefore("TB1015351632", timestamp + 10 * MS)
      logger.info { "got quotes before ${timestamp + 10}: $list" }
      assertEquals(1, list.size)
      val quote = list[0]
      assertEquals("3.1234", quote.price.toPlainString())
      assertEquals(timestamp, quote.timestamp)
      context.completeNow()
    }


  @Test
  fun `get quotes before timestamp should return ordered list`(vertx: Vertx, context: VertxTestContext) =
    runBlocking(vertx.dispatcher()) {
      val timestamp = 1621805854000L // Date and time (GMT): Sunday, May 23, 2021 9:37:34 PM
      val isin = "TB1015351632"
      redis.apply {
        addQuoteToQueue(isin, "1.2", timestamp + 100 * MS)
        addQuoteToQueue(isin, "3.122", timestamp + 150 * MS)
        addQuoteToQueue(isin, "2.21", timestamp + 10 * MS)
        addQuoteToQueue(isin, "5.22345", timestamp + 170 * MS)
        addQuoteToQueue(isin, "4.23456", timestamp + 160 * MS)
        addQuoteToQueue(isin, "6.7890", timestamp + 200 * MS)
      }

      // the result is ordered by timestamp, not the order of being inserted
      val list = redis.getQuoteBefore(isin, timestamp + 190 * MS)
      assertEquals(5, list.size)
      val other = list.map { (it.timestamp - timestamp) / MS }.toList()
      assertEquals(10, other[0])
      assertEquals(100, other[1])
      assertEquals(150, other[2])
      assertEquals(160, other[3])
      assertEquals(170, other[4])
      context.completeNow()
    }

  @Test
  fun `get non exist candle should return null`(vertx: Vertx, context: VertxTestContext) =
    runBlocking(vertx.dispatcher()) {
      val timestamp = 1621849270000L // Date and time (GMT): Monday, May 24, 2021 9:41:10 AM
      val isin = "TB1015351632"
      val candle = redis.getCandle(isin, timestamp)
      assertNull(candle)
      context.completeNow()
    }

  @Test
  fun `remove quotes before timestamp should be OK`(vertx: Vertx, context: VertxTestContext) =
    runBlocking(vertx.dispatcher()) {
      val timestamp = 1621805854000L // Date and time (GMT): Sunday, May 23, 2021 9:37:34 PM
      val isin = "TB1015351632"
      redis.apply {
        addQuoteToQueue(isin, "1.2", timestamp + 100 * MS)
        addQuoteToQueue(isin, "3.122", timestamp + 150 * MS)
        addQuoteToQueue(isin, "2.21", timestamp + 10 * MS)
        addQuoteToQueue(isin, "5.22345", timestamp + 170 * MS)
        addQuoteToQueue(isin, "4.23456", timestamp + 160 * MS)
        addQuoteToQueue(isin, "6.7890", timestamp + 200 * MS)
      }

      redis.removeQuotesBefore(isin, timestamp + 190 * MS)

      val quotes = redis.getQuoteBefore(isin, timestamp + 250 * MS)
      assertEquals(1, quotes.size)
      assertEquals(timestamp + 200 * MS, quotes[0].timestamp)
      context.completeNow()
    }

  @Test
  fun `combine two candles should be OK`(vertx: Vertx, context: VertxTestContext) =
    runBlocking(vertx.dispatcher()) {
      val isin = "TB1015351632"
      val timestamp = 1621805854000L // Date and time (GMT): Sunday, May 23, 2021 9:37:34 PM

      val one = Candle.fromSingleQuote(Quote(isin, BigDecimal("2.123405"), timestamp))
      val other = Candle.fromSingleQuote(Quote(isin, BigDecimal("3.2098"), timestamp + 10 * SEC))

      val combined = combineCandle(one, other)

      assertEquals(isin, combined.isin)
      assertEquals(one.openTimestamp, combined.openTimestamp)
      assertEquals(one.openTimestampInternal, combined.openTimestampInternal)
      assertEquals(one.openPrice, combined.openPrice)
      assertEquals(one.lowPrice, combined.lowPrice)
      assertEquals(other.highPrice, combined.highPrice)
      assertEquals(other.closePrice, combined.closePrice)
      assertEquals(other.closeTimestamp, combined.closeTimestamp)
      assertEquals(other.closeTimestampInternal, combined.closeTimestampInternal)

      context.completeNow()
    }


  @Test
  fun `fill candle gaps should be OK`(vertx: Vertx, context: VertxTestContext) =
    runBlocking(vertx.dispatcher()) {
      val isin = "TB1015351632"
      val oneTime = 1621805854000L // Date and time (GMT): Sunday, May 23, 2021 9:37:34 PM

      val anotherTime = oneTime + 5 * MIN
      val minuteFrom = minutesSinceEpoch(oneTime)
      redis.fillGapCandles(isin, BigDecimal("3.141509"), minuteFrom, minutesSinceEpoch(anotherTime))
      val keys = redis.candleKeysOf(isin)
      assertEquals(6, keys.size)
      assertTrue(
        keys.containsAll(
          listOf(
            "candle:$isin:${minuteFrom}",
            "candle:$isin:${minuteFrom + 1}",
            "candle:$isin:${minuteFrom + 2}",
            "candle:$isin:${minuteFrom + 3}",
            "candle:$isin:${minuteFrom + 4}",
            "candle:$isin:${minuteFrom + 5}"
          )
        )
      )
      context.completeNow()
    }

  @Test
  fun `batch process 2 should be OK`(vertx: Vertx, context: VertxTestContext) =
    runBlocking(vertx.dispatcher()) {

      val timestamp = 1621805854000L // Date and time (GMT): Sunday, May 23, 2021 9:37:34 PM
      val isin = "TB1015351632"
      redis.apply {
        addQuoteToQueue(isin, "1.2", timestamp + 100 * MS)
        addQuoteToQueue(isin, "3.122", timestamp + 150 * MS)
        addQuoteToQueue(isin, "2.21", timestamp + 10 * MS)
        addQuoteToQueue(isin, "5.22345", timestamp + 170 * MS)
        addQuoteToQueue(isin, "4.23456", timestamp + 160 * MS)
        addQuoteToQueue(isin, "6.789", timestamp + 200 * MS)
        addQuoteToQueue(isin, "6.0890", timestamp + 300 * MS)
        addQuoteToQueue(isin, "6.2890", timestamp + 400 * MS)
        addQuoteToQueue(isin, "6.1890", timestamp + 500 * MS)

      }

      redis.batchProcess2(isin, timestamp + 1 * SEC)
      val candles = redis.candleKeysOf(isin)
      assertEquals(1, candles.size)
      val candle = redis.getCandleByKey(candles[0])
      assertEquals(openTimestampOf(timestamp), candle.openTimestamp)
      assertEquals(timestamp + 10 * MS, candle.openTimestampInternal)
      assertEquals("2.21", candle.openPrice.toPlainString())
      assertEquals("6.789", candle.highPrice.toPlainString())
      assertEquals("1.2", candle.lowPrice.toPlainString())
      assertEquals("6.1890", candle.closePrice.toPlainString())
      assertEquals(timestamp + 500 * MS, candle.closeTimestampInternal)
      assertEquals(closeTimestampOf(timestamp), candle.closeTimestamp)

      context.completeNow()
    }

  @Test
  fun `batch process should be OK`(vertx: Vertx, context: VertxTestContext) =
    runBlocking(vertx.dispatcher()) {

      val timestamp = 1621805854000L // Date and time (GMT): Sunday, May 23, 2021 9:37:34 PM
      val isin = "TB1015351632"
      redis.apply {
        addQuoteToQueue(isin, "1.2", timestamp + 100 * MS)
        addQuoteToQueue(isin, "3.122", timestamp + 150 * MS)
        addQuoteToQueue(isin, "2.21", timestamp + 10 * MS)
        addQuoteToQueue(isin, "5.22345", timestamp + 170 * MS)
        addQuoteToQueue(isin, "4.23456", timestamp + 160 * MS)
        addQuoteToQueue(isin, "6.789", timestamp + 200 * MS)
        addQuoteToQueue(isin, "6.0890", timestamp + 300 * MS)
        addQuoteToQueue(isin, "6.2890", timestamp + 400 * MS)
        addQuoteToQueue(isin, "6.1890", timestamp + 500 * MS)

      }

      redis.batchProcess(isin, timestamp + 1 * SEC)
      val candles = redis.candleKeysOf(isin)
      assertEquals(1, candles.size)
      val candle = redis.getCandleByKey(candles[0])
      assertEquals(openTimestampOf(timestamp), candle.openTimestamp)
      assertEquals(timestamp + 10 * MS, candle.openTimestampInternal)
      assertEquals("2.21", candle.openPrice.toPlainString())
      assertEquals("6.789", candle.highPrice.toPlainString())
      assertEquals("1.2", candle.lowPrice.toPlainString())
      assertEquals("6.1890", candle.closePrice.toPlainString())
      assertEquals(timestamp + 500 * MS, candle.closeTimestampInternal)
      assertEquals(closeTimestampOf(timestamp), candle.closeTimestamp)

      context.completeNow()
    }

  @Test
  fun `delete instrument should be OK`(vertx: Vertx) =
    runBlocking(vertx.dispatcher()) {
      val isin = "TB1015351631"
      val timestamp = 1621805854000L // Date and time (GMT): Sunday, May 23, 2021 9:37:34 PM

      redis.addInstrument(isin, "ark k")
      redis.updatePrice(isin, BigDecimal("1.234"))
      redis.addQuoteToQueue(isin, BigDecimal("2.3456"), timestamp)
      redis.insertCandle(Candle.fromSingleQuote(Quote(isin, BigDecimal("3.456"), timestamp - 1 * MIN)))

      val description = redis.getDescription(isin)
      assertEquals("ark k", description)
      val toPlainString = redis.getPrice(isin)?.toPlainString()
      assertEquals("1.234", toPlainString)
      val quoteBefore = redis.getQuoteBefore(isin, timestamp + 1 * MS)
      assertEquals(Quote(isin, BigDecimal("2.3456"), timestamp), quoteBefore[0])
      val candle = redis.getCandle(isin, timestamp - 1 * MIN)
      assertNotNull(candle)

      redis.deleteInstrument(isin)

      assertNull(redis.getPrice(isin))
      assertFalse(redis.containsISIN(isin))
      assertNull(redis.getDescription(isin))
      assertTrue(redis.getQuoteBefore(isin, timestamp + 1 * MS).isEmpty())
      assertNull(redis.getCandle(isin, timestamp - 1 * MIN))

    }

  @Test
  fun `get candles in last 30 minutes should be Ok`(vertx: Vertx) =
    runBlocking(vertx.dispatcher()) {
      val isin = "TB1015351631"
      val timestamp = 1621805854000L // Date and time (GMT): Sunday, May 23, 2021 9:37:34 PM

      (1..30).forEach {
        val q = Candle.fromSingleQuote(Quote(isin, BigDecimal("1.043"), timestamp + it * MIN))
        redis.insertCandle(q)
      }

      assertEquals(30, redis.getCandlesOf(isin).size)
    }
}
